package Snake;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.util.Random;

import javax.swing.JFrame;

public class Game extends Canvas implements Runnable,KeyListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int WIDTH = 160;
	public static int HEIGHT = 160;
	public static int SCALE = 3;
	public Cobra[] cobra = new Cobra[30];
	
	public int score = 0;
	
	public int bolaX = 0,bolaY=0;;
	
	public int speed = 1;
	
	public boolean right,left,up,down;
	
	public Game() {
		this.setPreferredSize(new Dimension(WIDTH*SCALE ,HEIGHT*SCALE));
		for(int i = 0 ;  i<cobra.length ; i++) {
			cobra[i] = new Cobra(0,0);
			
			
		}
		this.addKeyListener(this);
	}
	
	public void tick() {
		for(int i = cobra.length-1 ;  i>0 ; i--) {
			cobra[i].x = cobra[i-1].x;
			cobra[i].y = cobra[i-1].y;
			
			
		}
		
		if(cobra[0].x+10 <0) {
			cobra[0].x = 480;
			
		}else if(cobra[0].x+10 >=480) {
			cobra[0].x = -10;
			
		}
		
		if(cobra[0].y+10 <0) {
			cobra[0].y = 480;
			
		}else if(cobra[0].y+10 >=480) {
			cobra[0].y = -10;
			
		}
		if(right) {
			cobra[0].x+=speed;
		}else if(left) {
			cobra[0].x-=speed;
		}else if(up) {
			cobra[0].y-=speed;
		}else if(down) {
			cobra[0].y+=speed;
		}
		
		
		if(new Rectangle(cobra[0].x,cobra[0].y,10,10).intersects(new Rectangle (bolaX,bolaY,10,10))) {
			bolaX = new Random().nextInt(480-10);
			bolaY = new Random().nextInt(480-10);
			score ++;
			speed++;
			System.out.println("pontuacao"+ score);
				
			
		}
	}
	public void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if(bs==null) {
			this.createBufferStrategy(3);
			return;
		}
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH*SCALE, HEIGHT*SCALE);
		
		for(int i = 0 ;  i<cobra.length ; i++) {
			g.setColor(Color.blue);
			g.fillRect(cobra[i].x,cobra[i].y, 10, 10);
			
		}
		g.setColor(Color.red);
		g.fillRect(bolaX, bolaY, 10, 10);
		g.dispose();
		bs.show();
		
	}
	public static void main(String[] args) {
		Game game = new Game();
		JFrame frame= new JFrame("Snake");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(game);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		new Thread(game).start();
	}
	public void run() {
		while(true) {
			render();
			tick();
			try {
				Thread.sleep(1000/60);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			right = true;
			left = false;
			up = false;
			down = false;
		}
		else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			left = true;
			right = false;
			up = false;
			down = false;
			
		}else if(e.getKeyCode() == KeyEvent.VK_UP) {
			up = true;
			right = false;
			left = false;
			down = false;
			
		}
		else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			down = true;
			right = false;
			left = false;
			up = false;
			
			
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	

}